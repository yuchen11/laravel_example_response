<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;

use App\Http\Resources\UserResource;

use Jiannei\Response\Laravel\Support\Facades\Response;

class UserController extends Controller
{
    public function getUser()
    {
        return Response::success(UserResource::collection(User::get()));
    }

    public function getUserPage()
    {
        return Response::success(UserResource::collection(User::paginate(5, ['*'], 'page', 1)));
    }
}
