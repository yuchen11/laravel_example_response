<?php

namespace App\Repositories\Enums;

use Jiannei\Enum\Laravel\Repositories\Enums\HttpStatusCodeEnum as BaseResponseCodeEnum;

class ResponseCodeEnum extends BaseResponseCodeEnum
{
    //通用錯誤
    const DATA_NOT_EXIST = 4049999;
    const API_PARAMS_ERROR = 4009999;
}