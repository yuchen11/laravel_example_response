<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/response')->group(function(){
    Route::get('error_4049999', [\App\Http\Controllers\Api\ErrorController::class, 'error_4049999']);

    Route::get('error_500', [\App\Http\Controllers\Api\ErrorController::class, 'error_500']);

    Route::get('error_500_2', [\App\Http\Controllers\Api\ErrorController::class, 'error_500_2']);
});

Route::prefix('/user')->group(function(){
    Route::get('/', [\App\Http\Controllers\Api\UserController::class, 'getUser']);
    Route::get('/page', [\App\Http\Controllers\Api\UserController::class, 'getUserPage']);
});