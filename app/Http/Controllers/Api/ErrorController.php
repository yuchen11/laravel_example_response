<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Jiannei\Response\Laravel\Support\Facades\Response;

class ErrorController extends Controller
{
    public function error_4049999()
    {
        abort(4049999);
    }

    public function error_500()
    {
        abort(500);
    }

    public function error_500_2()
    {
        abort(500, '自訂錯誤訊息');
    }
}
